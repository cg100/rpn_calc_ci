with import <nixpkgs> {};

derivation {
  name = "rpnBuilderDerivation";
  builder = "${pkgs.bash}/bin/bash";
  args = [ ./builder.sh ];
  gcc = pkgs.gcc;
  system = builtins.currentSystem;
  src = ./.;
  make = "${pkgs.gnumake}/bin/make";
  coreutils = pkgs.coreutils;
  cp = "${pkgs.coreutils}/bin/cp"; 
}
