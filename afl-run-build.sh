export PATH="$coreutils/bin:$afl/bin:$gcc/bin"

$cp -r $src/* .
mkdir ./out
afl-fuzz -i ./test/in -o ./out $fuzzer  &
FUZZER_PID=$!
sleep 20
kill $FUZZER_PID 
$cp -r ./out $out

