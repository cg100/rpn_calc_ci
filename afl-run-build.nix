with import <nixpkgs> {};

let
  fuzzer = import ./afl-build.nix;
in derivation {
  inherit fuzzer;
  name = "rpnBuilderDerivation";
  builder = "${pkgs.bash}/bin/bash";
  args = [ ./afl-run-build.sh ];
  afl = pkgs.afl;
  gcc = pkgs.gcc;
  system = builtins.currentSystem;
  src = ./.;
  make = "${pkgs.gnumake}/bin/make";
  coreutils = pkgs.coreutils;
  cp = "${pkgs.coreutils}/bin/cp";
}
