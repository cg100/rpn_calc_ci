with import <nixpkgs.nix> {};

{
  binary = import ./build.nix;
  tests = import ./test-build.nix;
  afl-build = import ./afl-build.nix;
  afl-run = import ./afl-run-build.nix;
}
