export PATH="$coreutils/bin:$gcc/bin"
export CXXFLAGS="-I $catch2/include -fsanitize=address"

declare -xp

$cp -r $src/* .
$make bin/tests 
$cp ./bin/tests $out

