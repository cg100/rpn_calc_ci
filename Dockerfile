FROM ubuntu:latest

RUN apt-get update && \
    apt-get -y install afl && \
    apt-get -y install build-essential && \
    apt-get -y install curl


COPY ./ /root/

WORKDIR /root

RUN mkdir /usr/include/catch2 && curl https://raw.githubusercontent.com/catchorg/Catch2/master/single_include/catch2/catch.hpp >> /usr/include/catch2/catch.hpp

RUN make bin/tests

